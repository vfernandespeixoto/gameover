# GAME-OVER
![Image of GameOVER](https://definicao.net/wp-content/uploads/2019/05/game-over-2.jpg)

### Build/Setup do projeto

#### Pré-requisitos

* Git
* JAVA 8
* Maven 3

#### Clone o projeto

	git clone https://gitlab.com/vfernandespeixoto/gameover.git

#### Build do projeto maven:

	mvn clean install
	
#### Executando os testes unitários
	
	mvn clean test - Executa  os testes
	google-chrome target/site/jacoco/index.html - Exibi o resultado de cobertura dos testes

#### Execute a aplicação

	java -jar ./target/gameover-1.0-SNAPSHOT.jar
 
#### Arquivo de Log

    O projeto le por default o arquivo games.log que fica na pasta resource, 
    caso queira ler outro arquivo basta alterar a propriedade 
    logfile.path= do arquivo application.properties 

#### Documentação do projeto

	http://0.0.0.0:8080/swagger-ui.html

#### Rotas
    
    http://0.0.0.0:8080/v1/healthcheck - Valida a aplicação e a leitura do arquivo
	http://0.0.0.0:8080/v1/games - Faz a leitura do arquivo de log e retorna os games e uma lista
