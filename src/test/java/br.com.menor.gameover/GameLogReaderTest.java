package br.com.menor.gameover;

import br.com.menor.gameover.businnes.GameLogReaderBusinnes;
import br.com.menor.gameover.http.entity.GameResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GameLogReaderTest {


    @Autowired
    private GameLogReaderBusinnes gameLogReaderBusinnes;

    @Test
    public void getGames () throws Exception {

        try {
            List<GameResponse> games = gameLogReaderBusinnes.getGames();
            Assert.isTrue(games.size() == 21);

            GameResponse game1 = games.get(0);
            Assert.isTrue(game1.getId().equals(1));
            Assert.isTrue(game1.getTotalKills().equals(0));
            Assert.isTrue(game1.getKills().equals(new ArrayList<>()));
            Assert.isTrue(game1.getPlayers().equals(new ArrayList<>()));

            GameResponse game2 = games.get(1);
            Assert.isTrue(game2.getId().equals(2));
            Assert.isTrue(game2.getTotalKills().equals(11));
            Assert.isTrue(game2.getKills().get(0).getNickName().equals("Isgalamido"));
            Assert.isTrue(game2.getKills().get(0).getTotalKills().equals(-5));
            Assert.isTrue(game2.getKills().get(1).getNickName().equals("Mocinha"));
            Assert.isTrue(game2.getKills().get(1).getTotalKills().equals(0));

            GameResponse game21 = games.get(20);
            Assert.isTrue(game21.getId().equals(21));
            Assert.isTrue(game21.getTotalKills().equals(131));
            Assert.isTrue(game21.getKills().get(0).getNickName().equals("Dono da Bola"));
            Assert.isTrue(game21.getKills().get(0).getTotalKills().equals(14));
            Assert.isTrue(game21.getKills().get(1).getNickName().equals("Isgalamido"));
            Assert.isTrue(game21.getKills().get(1).getTotalKills().equals(17));
            Assert.isTrue(game21.getKills().get(2).getNickName().equals("Zeh"));
            Assert.isTrue(game21.getKills().get(2).getTotalKills().equals(19));
            Assert.isTrue(game21.getKills().get(3).getNickName().equals("Oootsimo"));
            Assert.isTrue(game21.getKills().get(3).getTotalKills().equals(22));
            Assert.isTrue(game21.getKills().get(4).getNickName().equals("Mal"));
            Assert.isTrue(game21.getKills().get(4).getTotalKills().equals(6));
            Assert.isTrue(game21.getKills().get(5).getNickName().equals("Assasinu Credi"));
            Assert.isTrue(game21.getKills().get(5).getTotalKills().equals(19));

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

    }
}
