package br.com.menor.gameover.utils;

import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

@Component
public class TestUtils {

    public String getStringFromFilePath(String filePath) throws IOException {
        File resource = new ClassPathResource(filePath).getFile();
        return new String(Files.readAllBytes(resource.toPath()));
    }
}
