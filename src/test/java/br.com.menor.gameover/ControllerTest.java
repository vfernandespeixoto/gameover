package br.com.menor.gameover;

import br.com.menor.gameover.businnes.GameLogReaderBusinnes;
import br.com.menor.gameover.businnes.HealthcheckBusinnes;
import br.com.menor.gameover.http.entity.GameKillsRespose;
import br.com.menor.gameover.http.entity.GameResponse;
import br.com.menor.gameover.http.entity.HealthcheckResponse;
import br.com.menor.gameover.utils.TestUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;


@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private TestUtils utils;

    @MockBean
    private GameLogReaderBusinnes gameLogReaderBusinnes;

    @MockBean
    private HealthcheckBusinnes healthcheckBusinnes;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    private List<GameResponse> getLogsMock () {
        List<GameResponse> games = new ArrayList<>();
        GameResponse game1 = new GameResponse();
        GameResponse game2 = new GameResponse();
        GameResponse game3 = new GameResponse();
        GameResponse game4 = new GameResponse();
        games.add(game1);
        games.add(game2);
        games.add(game3);
        games.add(game4);

        List<String> players1 = new ArrayList<>();
        List<String> players2 = new ArrayList<>();
        List<String> players3 = new ArrayList<>();
        List<String> players4 = new ArrayList<>();

        players1.add("menor");
        players1.add("menor2");
        players2.add("menor3");
        players2.add("menor4");
        players3.add("menor");
        players4.add("menor5");

        List<GameKillsRespose> kills1 = new ArrayList<>();
        List<GameKillsRespose> kills2 = new ArrayList<>();
        List<GameKillsRespose> kills3 = new ArrayList<>();
        List<GameKillsRespose> kills4 = new ArrayList<>();

        kills1.add(new GameKillsRespose());
        kills1.add(new GameKillsRespose());
        kills2.add(new GameKillsRespose());
        kills2.add(new GameKillsRespose());
        kills3.add(new GameKillsRespose());
        kills4.add(new GameKillsRespose());

        kills1.get(0).setTotalKills(5);
        kills1.get(0).setNickName("menor");
        kills1.get(1).setTotalKills(4);
        kills1.get(1).setNickName("menor2");
        kills2.get(0).setTotalKills(3);
        kills2.get(0).setNickName("menor3");
        kills2.get(1).setTotalKills(-10);
        kills2.get(1).setNickName("menor4");
        kills3.get(0).setTotalKills(3);
        kills3.get(0).setNickName("menor");
        kills4.get(0).setTotalKills(-1);
        kills4.get(0).setNickName("menor5");


        games.get(0).setId(1);
        games.get(0).setTotalKills(10);
        games.get(0).setPlayers(players1);
        games.get(0).setKills(kills1);

        games.get(1).setId(2);
        games.get(1).setTotalKills(11);
        games.get(1).setPlayers(players2);
        games.get(1).setKills(kills2);

        games.get(2).setId(3);
        games.get(2).setTotalKills(12);
        games.get(2).setPlayers(players3);
        games.get(2).setKills(kills3);

        games.get(3).setId(4);
        games.get(3).setTotalKills(13);
        games.get(3).setPlayers(players4);
        games.get(3).setKills(kills4);

        return games;
    }

    private HealthcheckResponse getHealthMock () {

        HealthcheckResponse healthcheckResponse = new HealthcheckResponse();

        healthcheckResponse.setFindFile(true);
        healthcheckResponse.setVersion("0.0.0");
        healthcheckResponse.setServer("localhost");
        healthcheckResponse.setFile("games.log");

        return healthcheckResponse;
    }
    @Test
    public void getGames200() throws Exception {

        when(gameLogReaderBusinnes.getGames()).thenReturn(getLogsMock());

        MockHttpServletRequestBuilder builder =
                MockMvcRequestBuilders.get("/v1/games")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .characterEncoding("UTF-8");

        MvcResult result = this.mockMvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status()
                        .isOk())
                .andDo(MockMvcResultHandlers.print())
                .andReturn();

        String resultContent = result.getResponse().getContentAsString();
        JSONArray response = new JSONArray(resultContent);
        Assert.assertEquals(response.toString(), utils.getStringFromFilePath("get_games_200.json"));

    }

    @Test
    public void getGames500() throws Exception {

        when(gameLogReaderBusinnes.getGames()).thenThrow(new Exception());

        MockHttpServletRequestBuilder builder =
                MockMvcRequestBuilders.get("/v1/games")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .characterEncoding("UTF-8");
        try {
            MvcResult result = this.mockMvc.perform(builder)
                    .andExpect(MockMvcResultMatchers.status()
                            .is5xxServerError())
                    .andDo(MockMvcResultHandlers.print())
                    .andReturn();
        }catch (Exception e) {
            Assert.assertEquals(e.getMessage(),  "Request processing failed; nested exception is java.lang.Exception");
        }

    }

    @Test
    public void getHealthcheck200() throws Exception {

        when(healthcheckBusinnes.healthcheck()).thenReturn(getHealthMock());

        MockHttpServletRequestBuilder builder =
                MockMvcRequestBuilders.get("/v1/healthcheck")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .characterEncoding("UTF-8");

        MvcResult result = this.mockMvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status()
                        .isOk())
                .andDo(MockMvcResultHandlers.print())
                .andReturn();

        String resultContent = result.getResponse().getContentAsString();
        JSONObject response = new JSONObject(resultContent);
        Assert.assertEquals(response.toString(), utils.getStringFromFilePath("get_health_200.json"));

    }

    @Test
    public void getHealthcheck500() throws Exception {

        HealthcheckResponse healthcheckResponse = getHealthMock();
        healthcheckResponse.setFindFile(false);
        when(healthcheckBusinnes.healthcheck()).thenReturn(healthcheckResponse);

        MockHttpServletRequestBuilder builder =
                MockMvcRequestBuilders.get("/v1/healthcheck")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .characterEncoding("UTF-8");

        MvcResult result = this.mockMvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status()
                        .isInternalServerError())
                .andDo(MockMvcResultHandlers.print())
                .andReturn();

        String resultContent = result.getResponse().getContentAsString();
        JSONObject response = new JSONObject(resultContent);
        Assert.assertEquals(response.toString(), utils.getStringFromFilePath("get_health_500.json"));

    }
}
