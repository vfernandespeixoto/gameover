package br.com.menor.gameover.http.entity;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class GameResponse implements Serializable {

    private Integer id;

    private Integer totalKills;

    private List<String> players;

    private List<GameKillsRespose> kills;

    public GameResponse() {
        this.id = 0;
        this.totalKills = 0;
        List<String> players = new ArrayList<>();
        this.setPlayers(players);
        List<GameKillsRespose> kills = new ArrayList<>();
        this.setKills(kills);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTotalKills() {
        return totalKills;
    }

    public void setTotalKills(Integer totalKills) {
        this.totalKills = totalKills;
    }

    public List<String> getPlayers() {
        return players;
    }

    public void setPlayers(List<String> players) {
        this.players = players;
    }

    public List<GameKillsRespose> getKills() {
        return kills;
    }

    public void setKills(List<GameKillsRespose> kills) {
        this.kills = kills;
    }
}
