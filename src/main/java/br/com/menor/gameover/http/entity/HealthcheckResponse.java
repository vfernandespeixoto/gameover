package br.com.menor.gameover.http.entity;

import java.io.Serializable;

public class HealthcheckResponse implements Serializable {

    private String version;

    private String server;

    private String file;

    private Boolean findFile;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public Boolean getFindFile() {
        return findFile;
    }

    public void setFindFile(Boolean findFile) {
        this.findFile = findFile;
    }
}
