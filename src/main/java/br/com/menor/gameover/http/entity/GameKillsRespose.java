package br.com.menor.gameover.http.entity;

import java.io.Serializable;
import java.math.BigInteger;

public class GameKillsRespose implements Serializable {

    private String nickName;

    private Integer totalKills;

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public Integer getTotalKills() {
        return totalKills;
    }

    public void setTotalKills(Integer totalKills) {
        this.totalKills = totalKills;
    }
}
