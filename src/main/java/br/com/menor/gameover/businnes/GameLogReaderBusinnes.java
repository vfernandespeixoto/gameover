package br.com.menor.gameover.businnes;

import br.com.menor.gameover.configuration.ConfigKeys;
import br.com.menor.gameover.http.entity.GameKillsRespose;
import br.com.menor.gameover.http.entity.GameResponse;
import br.com.menor.gameover.utils.ObjectValidation;
import ch.qos.logback.classic.Logger;
import net.logstash.logback.marker.Markers;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class GameLogReaderBusinnes {

    Logger LOGGER = (Logger) LoggerFactory.getLogger(GameLogReaderBusinnes.class);

    @Autowired
    private ConfigKeys configKeys;

    private static final String NICK_WORLD = "<world>";
    private static final String INIT_GAME = "InitGame:";
    private static final String KILL = "Kill:";
    private static final String KILLED = " killed ";
    private static final String PATH_LOGFILE_STATIC = "/games.log";

    public BufferedReader readerLog() throws Exception, FileNotFoundException {

        BufferedReader reader = null;

        try {

            if(ObjectValidation.isEmptyOrNull(configKeys.getGameLogPath())) {

                InputStream in = getClass().getResourceAsStream(PATH_LOGFILE_STATIC);
                reader = new BufferedReader(new InputStreamReader(in));

            } else {

                Path path = Paths.get(configKeys.getGameLogPath());
                reader = new BufferedReader(new FileReader(path.toFile()));

            }

        } catch (FileNotFoundException e) {
            LOGGER.error(new StringBuilder().append("FILE ")
                    .append(!ObjectValidation.isEmptyOrNull(configKeys.getGameLogPath()) ?
                            configKeys.getGameLogPath() : PATH_LOGFILE_STATIC)
                    .append(" FileNotFoundException").toString());
            throw e;
        } catch (Exception e) {
            throw e;
        }

        return reader;
    }

    private static String getNickKill(final String line) {
        try {
            String [] contents = line.split(KILLED);
            if (contents[1]==null) {
                throw new IllegalStateException();
            }
            String killerPart = contents[0];
            contents = killerPart.split(":");
            return contents[contents.length-1].trim();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String getNickDead(String line) {
        try {
            String [] contents = line.split(KILLED);
            String deadPart = contents[1];
            contents = deadPart.split(" by ");
            return contents[0].trim();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private void removeNickWorld(List<GameResponse> games) {

        for (GameResponse game : games) {

            if (!ObjectValidation.isEmptyOrNull(game.getKills())) {
                List<GameKillsRespose> killsNotWord =  game.getKills().stream()
                        .filter(c -> !c.getNickName().equals(NICK_WORLD))
                        .collect(Collectors.toList());

                game.setKills(killsNotWord);
            }
        }
    }

    private void setKills(GameResponse game, String nickKill, String nickDead) {

        if (ObjectValidation.isEmptyOrNull(game.getKills())) {
            GameKillsRespose new_kill = new GameKillsRespose();
            new_kill.setNickName(nickKill);
            new_kill.setTotalKills(0);
            game.setKills(new ArrayList<>());
            game.getKills().add(new_kill);
        }

        List<GameKillsRespose> killsByNickKill =  game.getKills().stream()
                .filter(c -> c.getNickName().equals(nickKill))
                .collect(Collectors.toList());

        List<GameKillsRespose> killsByNickDedad =  game.getKills().stream()
                .filter(c -> c.getNickName().equals(nickDead))
                .collect(Collectors.toList());

        if (!ObjectValidation.isEmptyOrNull(killsByNickKill)) {
            killsByNickKill.get(0).setTotalKills(killsByNickKill.get(0).getTotalKills() + 1);
        } else {
            GameKillsRespose new_kill = new GameKillsRespose();
            new_kill.setNickName(nickKill);
            new_kill.setTotalKills(1);
            game.getKills().add(new_kill);
        }

        if (ObjectValidation.isEmptyOrNull(killsByNickDedad)) {
            GameKillsRespose new_kill = new GameKillsRespose();
            new_kill.setNickName(nickDead);
            new_kill.setTotalKills(0);
            game.getKills().add(new_kill);
        }
    }

    private void removeKills(GameResponse game, String nickName) {

        List<GameKillsRespose> killsByNickName =  game.getKills().stream()
                .filter(c -> c.getNickName().equals(nickName))
                .collect(Collectors.toList());

        if (!ObjectValidation.isEmptyOrNull(killsByNickName)) {
            killsByNickName.get(0).setTotalKills(killsByNickName.get(0).getTotalKills() - 1);
        } else {
            GameKillsRespose new_kill = new GameKillsRespose();
            new_kill.setNickName(nickName);
            new_kill.setTotalKills(-1);
            game.getKills().add(new_kill);
        }
    }

    private void loadKills(GameResponse game, String nickKill, String nickDead) {

        setKills(game, nickKill, nickDead);

        if (nickKill.equals(NICK_WORLD)) {
            removeKills(game, nickDead);
        }
    }

    private void setPlayres (List<GameResponse> games) {

        for (GameResponse game : games) {
            if (!ObjectValidation.isEmptyOrNull(game.getKills())) {
                game.setPlayers(game.getKills()
                        .stream()
                        .map((GameKillsRespose kill) -> kill.getNickName())
                        .collect(Collectors.toList()));
            }
        }

    }

    public List<GameResponse> getGames() throws Exception {

        BufferedReader reader = readerLog();
        List<GameResponse> games = new LinkedList<>();
        GameResponse gameProcessing = null;


        Integer idGame = 0;
        Integer countLine = 0;
        String line = reader.readLine();

        while (line != null) {

            ++countLine;

            if(!ObjectValidation.isEmptyOrNull(line)) {

                if (line.contains(INIT_GAME)) {

                    gameProcessing = new GameResponse();
                    gameProcessing.setId(++idGame);
                    games.add(gameProcessing);

                    LOGGER.info(new StringBuilder().append("LINE ")
                            .append(countLine)
                            .append(" GAME_ID ")
                            .append(gameProcessing.getId())
                            .append("INIT NEW GAME").toString());

                } else if (line.contains(KILL)) {
                    String killerName = getNickKill(line);
                    String deadName = getNickDead(line);

                    if (ObjectValidation.isEmptyOrNull(gameProcessing.getTotalKills())) {
                        gameProcessing.setTotalKills(1);
                    } else {
                        gameProcessing.setTotalKills(gameProcessing.getTotalKills() + 1);
                    }

                    loadKills(gameProcessing, killerName, deadName);

                    LOGGER.info(new StringBuilder().append("LINE ")
                            .append(countLine)
                            .append(" GAME_ID ")
                            .append(gameProcessing.getId())
                            .append(" NICK_KILL ")
                            .append(killerName)
                            .append(" NICK_DEAD ")
                            .append(deadName)
                            .append(" PROCESS NEW KILL").toString());
                } else {
                    LOGGER.info(new StringBuilder().append("LINE ")
                            .append(countLine)
                            .append(" WORTHLESS LINE").toString());
                }

                line = reader.readLine();
            }

        }

        removeNickWorld(games);
        setPlayres(games);

        return games;
    }
}
