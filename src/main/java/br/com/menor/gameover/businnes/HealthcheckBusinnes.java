package br.com.menor.gameover.businnes;

import br.com.menor.gameover.configuration.ConfigKeys;
import br.com.menor.gameover.http.entity.HealthcheckResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.net.InetAddress;
import java.net.UnknownHostException;

@Component
public class HealthcheckBusinnes {

    @Autowired
    private ConfigKeys configKeys;

    @Autowired
    private GameLogReaderBusinnes gameLogReaderBusinnes;

    public HealthcheckResponse healthcheck() throws UnknownHostException {

        HealthcheckResponse healthcheck = new HealthcheckResponse();
        healthcheck.setVersion(configKeys.getApplicationVersion());
        healthcheck.setServer(InetAddress.getLocalHost().getHostAddress());
        healthcheck.setFile(configKeys.getGameLogPath());


        try {
            gameLogReaderBusinnes.readerLog();
            healthcheck.setFindFile(true);
        } catch (Exception e) {
            e.printStackTrace();
            healthcheck.setFindFile(false);
        }

        return healthcheck;

    }
}
