package br.com.menor.gameover.businnes;

import br.com.menor.gameover.http.entity.GameResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class GameOverBusinnes {

    @Autowired
    private GameLogReaderBusinnes gameLogReader;

    public List<GameResponse> getGames() throws Exception {

        return gameLogReader.getGames();

    }

}
