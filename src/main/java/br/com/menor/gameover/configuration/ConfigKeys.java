package br.com.menor.gameover.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ConfigKeys {

    @Value("${application.version}")
    private String applicationVersion;

    @Value("${game.log.path}")
    private String gameLogPath;

    public String getApplicationVersion() {
        return applicationVersion;
    }

    public String getGameLogPath() {
        return gameLogPath;
    }
}
