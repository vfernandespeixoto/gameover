package br.com.menor.gameover.controller;

import br.com.menor.gameover.businnes.HealthcheckBusinnes;
import br.com.menor.gameover.http.entity.HealthcheckResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.net.UnknownHostException;

@RestController
@RequestMapping("/v1/healthcheck")
public class HealthcheckController {

    @Autowired
    private HealthcheckBusinnes businnes;

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<HealthcheckResponse> health() throws UnknownHostException {

        HealthcheckResponse healthcheckResponse = businnes.healthcheck();

        HttpStatus status = HttpStatus.OK;

        if(!healthcheckResponse.getFindFile()) status=HttpStatus.INTERNAL_SERVER_ERROR;

        return new ResponseEntity<HealthcheckResponse>(healthcheckResponse, status);

    }
}