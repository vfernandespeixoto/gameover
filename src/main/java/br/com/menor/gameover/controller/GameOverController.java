package br.com.menor.gameover.controller;


import br.com.menor.gameover.businnes.GameOverBusinnes;
import br.com.menor.gameover.http.entity.GameResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE,
        value = "/v1/games")
public class GameOverController {

    @Autowired
    private GameOverBusinnes businnes;

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<GameResponse>> getGames() throws Exception  {

        return new ResponseEntity<List<GameResponse>>(businnes.getGames(), HttpStatus.OK);
    }
}
