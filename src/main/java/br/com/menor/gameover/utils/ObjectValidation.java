package br.com.menor.gameover.utils;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ObjectValidation {

    private static final Pattern PAT_BLANK = Pattern.compile("^\\s*$");

    public static boolean isEmptyOrNull(Object obj) {
        return obj == null;
    }

    public static boolean isEmptyOrNull(Long obj) {
        return obj == null || obj.equals(new Long("0")) || obj.equals(new Long(0));
    }

    public static boolean isEmptyOrNull(BigInteger obj) {
        return obj == null || obj.equals(new BigInteger("0"));
    }

    public static boolean isEmptyOrNull(String obj) {
        return obj == null || obj.trim().equals("");
    }

    public static boolean isEmptyOrNull(@SuppressWarnings("rawtypes") List obj) {
        return obj == null || obj.size() <= 0;
    }

    public static boolean isEmptyOrNull(@SuppressWarnings("rawtypes") Set obj) {
        return obj == null || obj.size() <= 0;
    }

    public static boolean isBlank(final String s) {

        if (isEmptyOrNull(s)) {

            return true;

        }

        Matcher m = PAT_BLANK.matcher(s);

        return m.matches();
    }

    public static boolean isNotBlank(final String s) {
        return !isBlank(s);
    }

    public static Long convertObjectToLong(final Object obj) {
        if (isEmptyOrNull(obj)) {
            return null;
        }
        return Long.valueOf(obj.toString().trim());
    }

    public static Double convertObjectToDouble(final Object obj) {
        if (isEmptyOrNull(obj)) {
            return null;
        }
        return Double.valueOf(obj.toString().trim());
    }

    public static Integer convertObjectToInteger(final Object obj) {
        if (isEmptyOrNull(obj)) {
            return null;
        }
        return Integer.valueOf(obj.toString().trim());
    }

    public static BigDecimal convertObjectToBigDecimal(final Object obj) {
        if (isEmptyOrNull(obj)) {
            return null;
        }
        return new BigDecimal(obj.toString().trim());
    }

    public static String convertObjectToString(final Object obj) {
        if (obj == null) {
            return null;
        }
        return obj.toString().trim();
    }

    public static boolean listIsEmptyOrNull(@SuppressWarnings("rawtypes") final List obj) {
        return (obj == null) || obj.isEmpty();
    }

}


